#include <iostream>
#include <HelloOperation.grpc.pb.h>
#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

int main()
{
	grpc_init();
	ClientContext context;
	auto sum_stub = SayHelloService::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));
	NameRequest nameRequest;
	std::cout << "Your name: ";
	std::string name;
	std::cin >> name;
	nameRequest.set_name(name);
	OperationResponse response;
	auto status = sum_stub->Salute(&context, nameRequest, &response);
}

