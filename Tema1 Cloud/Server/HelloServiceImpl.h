#pragma once
#include <HelloOperation.grpc.pb.h>

class HelloServiceImpl final : public SayHelloService::Service
{
public:
	HelloServiceImpl() {};
	::grpc::Status Salute(::grpc::ServerContext* context, const ::NameRequest* request, ::OperationResponse* response) override;


};

