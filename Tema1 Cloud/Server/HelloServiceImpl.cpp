#include "HelloServiceImpl.h"

::grpc::Status HelloServiceImpl::Salute(::grpc::ServerContext* context, const::NameRequest* request, ::OperationResponse* response)
{
	std::string message = request->name();
	std::cout << message<<"\n";
	return ::grpc::Status::OK;
}
